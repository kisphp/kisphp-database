<?php

error_reporting(E_ALL);
ini_set('error_reporting', 1);

require_once __DIR__ . '/../vendor/autoload.php';

$db = \Kisphp\Kisdb::getInstance();
$db->connect('localhost', 'root', 'root', 'test');
$db->enableDebug();

$createSql = "CREATE TABLE `users` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`email` VARCHAR(50) NOT NULL DEFAULT '',
	`password` VARCHAR(50) NOT NULL DEFAULT '',
	PRIMARY KEY (`id`)
)
ENGINE=InnoDB
;";

//dump($db);

function start($text)
{
    printf('<fieldset><legend>%s</legend>', $text);
}

function stop()
{
    echo '</fieldset>';
}

start('insert::last_insert_id');
$insert = $db->insert('users', [
    'email' => 'a_'.rand(1,1000).'@k.ro',
    'password' => 'password_a',
]);
dump($insert);
stop();

start('update::affected_rows');
$update = $db->update('users', [
    'email' => rand(1,10000) . '_example@k.ro',
], 2);
dump($update);
stop();

$a = $db->query("SELECT * FROM users ORDER BY id DESC LIMIT 3");

class User
{
    protected $id;
    protected $email;
    protected $password;
}

start('query::fetchObject');
while ($b = $a->fetchObject(User::class)) {
    dump($b);
}
stop();

start('getValue');
$email = $db->getValue("SELECT email FROM users WHERE id = 1");
dump($email);
stop();

start('getPairs');
$emailList = $db->getPairs("SELECT id, email FROM users");
dump($emailList);
stop();

start('getRow');
$row = $db->getRow("SELECT * FROM users WHERE id = 1");
dump($row);
stop();

dump($db->getLog());
